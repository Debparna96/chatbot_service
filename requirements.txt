###flask
click==7.1.2
Flask==1.1.2
itsdangerous==1.1.0
Jinja2==2.11.3
MarkupSafe==1.1.1
Werkzeug==1.0.1

rasa_nlu==0.15.1
spacy==2.0.18
#en_core_web_sm==2.0.0
